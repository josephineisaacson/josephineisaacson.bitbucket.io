/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Josephine Isaacson's ME405 Documentation", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Overview of Documentation", "index.html#sec_portfolio", null ],
    [ "A Synopsis", "index.html#sec_synopsis", null ],
    [ "Lab 00x01", "page_lab1.html", [
      [ "Lab 00x01 Source Code", "page_lab1.html#sec_lab1", null ],
      [ "Assignment description", "page_lab1.html#sec_lab1desc", null ]
    ] ],
    [ "Lab 00x02", "page_lab2button.html", [
      [ "Lab 00x02", "page_lab2button.html#sec_lab2button", null ],
      [ "Assignment Description", "page_lab2button.html#sec_lab2describe", null ],
      [ "Source Code", "page_lab2button.html#sec_lab00x02source", null ]
    ] ],
    [ "Lab 00x03", "page_lab3press.html", [
      [ "Lab00x03 Assignment description", "page_lab3press.html#sec_lab3press", null ]
    ] ],
    [ "Lab 00x04", "page_lab4temp.html", [
      [ "Assignment description", "page_lab4temp.html#sec_lab4Temperature", null ],
      [ "Temperature Plot", "page_lab4temp.html#sec_imagelab4", null ]
    ] ],
    [ "Term Project Calculations", "page_termproject.html", null ],
    [ "Lab 00x07", "page_lab7.html", [
      [ "Assignment Description", "page_lab7.html#sec_lab7", null ]
    ] ],
    [ "Term Project", "page_termproject2.html", [
      [ "Description", "page_termproject2.html#sec_termproject2", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"HW00x01_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';